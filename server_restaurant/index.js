var express = require("express");
var app = express();
var axios = require("axios");
var logger = require("morgan");
var cors = require("cors");
const bodyParser = require("body-parser");

var pedidos = [];

app.listen(3000, function () {
  console.log("server escuchando en http://localhost:3000");
});
app.use(bodyParser.json());
//app.use(cors);
app.use(logger("dev"));

app.post("/SolicitarPedido", (req, res) => {
  var pedido = req.body;
  pedido.Estado = "Recibido";
  pedido.id = pedidos.length + 1;
  pedidos.push(pedido);
  // Cambiamos el estado del pedido de Recibido a Preparado
  setTimeout(function () {
    //console.log(pedidos);
    let pedidoIndex = pedidos.findIndex((obj) => obj.id == pedido.id);
    //Update object's name property.
    pedidos[pedidoIndex].Estado = "Preparado";
    //console.log(pedidos);
    console.log("Pedido:  " + pedido.pedido + ", Preparado");
  }, 10000);

  /*    
    Enviar el pedido
    */
  setTimeout(() => {
    var pedidoIndex = pedidos.findIndex((obj) => obj.id == pedido.id);
    //Update object's name property.
    var pedidoEnviar = pedidos[pedidoIndex];
    axios
      .post("http://localhost:3001/EntregarPedido", pedidoEnviar)
      .then((respuesta) => {
        console.log("Pedido:  " + pedidoEnviar.pedido + ", listo para recoger");
      });
  }, 15000);

  res.send(pedido);
});

app.post("/VerificarEstado", (req, res) => {
  var id = req.body.id;
  console.log("Verificar Pedido:");
  var pedido1 = pedidos.filter((pedido) => pedido.id === id);
  console.log(pedido1);
  console.log("\n");
  res.send(pedido1[0]);
});
